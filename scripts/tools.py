from brownie import network, config, accounts

LOCAL_DEVELOPMENT_NETWORKS = ["ganache-local-cli"]


def get_account(key):
    if network.show_active() not in LOCAL_DEVELOPMENT_NETWORKS:
        return accounts.add(config["wallets"][network.show_active()][key])
    else:
        return accounts[config["wallets"][network.show_active()][key]]
