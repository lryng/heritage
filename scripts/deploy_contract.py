from brownie import Heritage, config, network, accounts
from scripts.tools import get_account

keys_special = ["contract_issuer", "testator"]
keys_payee = ["payee_1", "payee_2", "payee_3"]


def deploy_contract():

    shares_payee = [50, 30, 20]
    (
        account_issuer,
        account_testator,
    ) = list(map(get_account, keys_special))
    accounts_payee = list(map(get_account, keys_payee))

    heritage = Heritage.deploy(
        account_testator,
        accounts_payee,
        shares_payee,
        {"from": account_issuer},
    )


def main():
    deploy_contract()
