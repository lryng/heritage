from brownie import Heritage, network, accounts, config
from scripts.tools import get_account


def disable_heritage():
    account_testator = get_account("testator")
    heritage = Heritage[-1]
    txn = heritage.MoneyBack(heritage.balance(), {"from": account_testator})
    txn.wait(1)
    heritage.DisableHeritage({"from": account_testator})


def main():
    disable_heritage()
