from brownie import Heritage, config, network, accounts
from scripts.tools import get_account


def execute_heritage():
    account_issuer = get_account("contract_issuer")

    balance_before = account_issuer.balance()
    print(f"issuer's account balance before execution is {balance_before}")

    heritage = Heritage[-1]
    heritage.ExecuteHeritage({"from": account_issuer})

    balance_after = account_issuer.balance()
    print(f"issuer's account balance after execution is {balance_after}")

    if balance_after < balance_before:
        print("Gas cost is on issuer")


def main():
    execute_heritage()
