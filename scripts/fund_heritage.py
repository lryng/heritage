from brownie import Heritage, accounts, network, config
from scripts.tools import get_account


def fund(amount):
    account = get_account("contract_issuer")
    heritage = Heritage[-1]
    heritage.receiveFunds({"from": account, "value": amount})


def main():
    fund(10**18)
