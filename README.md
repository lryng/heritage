# heritage

Smart contract on Ethereum network for an on-chain implementation of a testator&#39;s heritage.

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Dependencies](#dependencies)
- [Package_Changes](#package_changes)
- [Contents](#contents)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [License](#license)

## Background

This is a proof-of-concept smart contract for uploading a person's heritage on a blockchain network. 

There are three kinds of roles here:
1. Contract issuer's account,
2. Testator's account,
3. Heir's(s') account(s).

Contract issuer deploys the heritage smart contract with the following input parameters:
1. Testator's account,
2. Array of heirs' accounts,
3. Array of heirs' shares with each share index corresponding to heir's account index from the previous array.

At deployment stage the testator is granted a role of `TESTATOR_ROLE`. This role ensures that only testator will be able to execute certain methods of the contract after its deployment.

After the deployment some amount of ETH must be sent to the heritage contract's account.

The heritage contract has three distinct states:
1. **Active** -- the heritage contract is currently active, additional funds and heirs can be added to the contract,
2. **Executed** -- the heritage contract has been executed and the funds have been transfered to the heirs' in accordance with their shares,
3. **Disabled** -- the heritage contract has been disabled with the funds being transfered to the testator's account.


## Install

1. [Brownie](https://eth-brownie.readthedocs.io/en/stable/install.html) is used to interact with the smart contract. 

```
pip install eth-brownie
```

2. For local development [ganache-cli](https://www.npmjs.com/package/ganache-cli) is used.

```
npm install -g ganache-cli
```


## Dependencies 

These are the packages used in the heritage contract. **Some changes are made** to these packages to ensure privacy of the testator's credentials. The changes to the packages are explained in the comments.
1. `@openzeppelin/contracts/access/Ownable.sol` -- standard package for `onlyOwner` modifier to the contract,
2. `@openzeppelin/contracts/utils/math/SafeMath.sol` -- safe math package.
3. `@openzeppelin/contracts/access/AccessControl.sol` -- package for assignment of specific roles to the contract's members. 

### Package_Changes

IMPORTANT CHANGES (marked with `ATTENTION!!!` in comments to the files):

1. Ownable.sol 
    - `owner() public` --> `owner() internal` to hide contract issuer's address

2. AccessControl.sol
    - `hasRole() public` --> `hasRole() internal` to avoid getting account's role by calling this method externally

3. IAccessControl.sol
    - `interface` --> `abstract contract` to make `hasRole() internal`
    - add `virtual` to each method in this file
    - `hasRole() external` --> `hasRole() internal`

## Contents

The project has the following files:

1. `./contracts/`
    - `Heritage.sol` -- the main contract file
    - `openzeppelin/` -- the dependencies folder
2. `./scripts/`
    - `deploy_contract.py` -- script for contract deployment
    - `fund_heritage.py` -- script for funding contract
    - `execute_heritage.py` -- script for heritage execution
    - `disable_heritage.py` -- script for heritage revocation 
3. `brownie-config.yaml` -- config file



## Usage

1. Force init brownie to add required folders:

```
brownie init -f
```

2. Compile contract:

```
brownie compile
```

3. Execute `ganache-cli` command with chainid `-i 1337` and keep it running:

```
ganache-cli -i 1337
```

4. Add a local network to list of brownie networks with corresponding `localhost`, `port`:

```
brownie networks add Ethereum ganache-local-cli host=http://<localhost> port=<port> chainid=1337
```

5. Deploy contract

```
brownie run scripts/deploy_contract.py --network ganache-local-cli
```

6. Fund contract's account

```
brownie run scripts/fund_heritage.py --network ganache-local-cli
```

7. Execute heritage

```
brownie run scripts/execute_heritage.py --network ganache-local-cli
```

8. ALTERNATIVE TO STEP 7, disable heritage

```
brownie run scripts/disable_heritage.py --network ganache-local-cli
```

## Maintainers

[@lryng](https://gitlab.com/lryng)


## License

MIT © 2022 Lora Young
