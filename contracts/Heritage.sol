pragma solidity ^0.8.15;

import "./openzeppelin/contracts/access/Ownable.sol";
import {SafeMath} from "./openzeppelin/contracts/utils/math/SafeMath.sol";
import "./openzeppelin/contracts/access/AccessControl.sol";

contract Heritage is Ownable, AccessControl {
    bytes32 public constant TESTATOR_ROLE = keccak256("TESTATOR_ROLE");

    enum State {
        Active,
        Executed,
        Disabled
    }
    State state;
    modifier isState(State _expectedState) {
        require(state == _expectedState);
        _;
    }

    event MultiTransfer(
        address indexed _from,
        uint256 indexed _value,
        address _to,
        uint256 _amount
    );

    event PayeeAdded(address account, uint256 shares);
    event PaymentCalculated(address account, uint256 amount);
    event AgreementFunded(address account, uint256 amount);

    address payable private testator;
    address payable[] private _payees;
    uint256 private _totalShares;
    mapping(address => uint256) private _shares;

    /*
    Contract deployment requires
    1. testator's account
    2. array of heirs' accounts
    3. array of heirs' shares  
     */
    constructor(
        address payable _testator,
        address payable[] memory payees,
        uint256[] memory shares_
    ) {
        testator = _testator;
        _grantRole(TESTATOR_ROLE, testator);
        require(
            payees.length == shares_.length,
            "Heritage: payees and shares length mismatch"
        );
        require(payees.length > 0, "Heritage: no payees");

        for (uint256 i = 0; i < payees.length; i++) {
            _addPayee(payees[i], shares_[i]);
        }
        state = State.Active;
    }

    /* 
    Requires msg.sender == TESTATOR and address(this).balance == 0
    Change state of a contract to disabled
     */
    function DisableHeritage()
        external
        isState(State.Active)
        onlyRole(TESTATOR_ROLE)
    {
        require(
            address(this).balance == 0,
            "Heritage: only zero-balance heritage can be disabled"
        );
        state = State.Disabled;
    }

    /* 
    Requires msg.sender == TESTATOR
    Transfer funds from contract's account to testator
     */
    function MoneyBack(uint256 _amount)
        external
        payable
        onlyRole(TESTATOR_ROLE)
        isState(State.Active)
    {
        uint256 totalAmount = address(this).balance;
        require(
            _amount >= totalAmount,
            "The value is not sufficient or exceed"
        );
        testator.transfer(_amount);
    }

    /* 
    Calculate payment to a heir based on their share and total amount of funds in heritage contract
     */
    function _calculatePayment(address _account, uint256 _totalAmount)
        private
        view
        returns (uint256)
    {
        return
            SafeMath.div(
                SafeMath.mul(_totalAmount, _shares[_account]),
                _totalShares,
                "Heritage: total share can't equal to zero"
            );
    }

    /* 
    Requires msg.sender == CONTRACT_ISSUER
    Run through a cycle and claculate payment for each heir
    Then execute _multiTransfer
     */
    function ExecuteHeritage()
        external
        payable
        onlyOwner
        isState(State.Active)
    {
        uint256 totalAmount = address(this).balance;
        require(totalAmount > 0, "Heritage should be greater than zero");

        uint256[] memory amounts = new uint256[](_payees.length);
        for (uint256 i = 0; i < _payees.length; i++) {
            uint256 amount = _calculatePayment(_payees[i], totalAmount);
            amounts[i] = amount;
            emit PaymentCalculated(_payees[i], amount);
        }

        _multiTransfer(_payees, amounts);
        state = State.Executed;
    }

    /* 
    Run through a cycle and transfer pre-calculated amount of funds to each heir
     */
    function _multiTransfer(
        address payable[] memory _addresses,
        uint256[] memory _amounts
    ) internal returns (bool) {
        for (uint256 i = 0; i < _addresses.length; i++) {
            _safeTransfer(_addresses[i], _amounts[i]);
            emit MultiTransfer(
                msg.sender,
                msg.value,
                _addresses[i],
                _amounts[i]
            );
        }
        return true;
    }

    function _safeTransfer(address payable _to, uint256 _amount) internal {
        require(_to != address(0));
        _to.transfer(_amount);
    }

    /* 
    This method is only called at contract deployment and adds a heir's address and share to corresponding arrays
     */
    function _addPayee(address payable account, uint256 shares_)
        private
        isState(State.Active)
    {
        require(account != address(0), "Heritage: account is the zero address");
        require(shares_ > 0, "Heritage: shares are 0");
        require(_shares[account] == 0, "Heritage: account already has shares");

        _payees.push(account);
        _shares[account] = shares_;
        _totalShares = _totalShares + shares_;
        emit PayeeAdded(account, shares_);
    }

    /* 
    Methods for funding the contract's account
     */
    receive() external payable isState(State.Active) {
        emit AgreementFunded(msg.sender, msg.value);
    }

    function receiveFunds() external payable isState(State.Active) {
        emit AgreementFunded(msg.sender, msg.value);
    }
}
